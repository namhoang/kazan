import React, { PureComponent } from 'react';
import AppBar from 'material-ui/AppBar';
import ToolBar from 'material-ui/Toolbar';

export default class Layout extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <AppBar>
                    <ToolBar/>
                </AppBar>
            </div>
        )
    }
}