import React from 'react';
import Layout from './layout/index';

export default class App extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return(<Layout/>)
    }
}